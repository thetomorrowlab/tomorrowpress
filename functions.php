<?php

################################################
#
#   TOMORROW PRESS FUNCTIONS
#
###############################################
#
#   Essential
#   - Clean calls
#   Easier theme development
#
#    - Clean wp_head
#   Remove non essential wordpress calls
#
#   - Disable file editing
#   Never edit files through wordpress
#
#   - Admin bar options
#    Show or hide admin bar for client
#
#   - Admin bar clean
#   Remove the wordpress stuff from the admin bar
#
#    - Menus
#
#    - Hide upgrade message from client
#
#    - SVG uploads
#
#    - Custom post types
#    Set custom post types for project
#
#   - Hide menus from client
#   Change these per project (Comment or uncomment)
#
#   - Change login screen
#   Enter brand colour and make sure logo.png links
#   - Image upload custom sizes
#   Set the custom image sizes, these should be decided at the design stage
#
#   Recommended (These will be commented out by default)
#   - Breadcrumbs
#   - Pagination
#   - Custom excerpt
#   - Remove comments from CMS
#
################################################

################################################
#
#  Clean calls
#
################################################
define('BASE_URL',get_bloginfo('url'));
define('THEME_URL',get_bloginfo('template_url'));
//define('CURRENT_PATH','//'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

################################################
#
#  Clean wp_head
#
################################################
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // index link
remove_action('wp_head', 'parent_post_rel_link'); // prev link
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'previous_post_link');
remove_action('wp_head', 'next_post_link');
remove_action('wp_head', 'post_permalink');
remove_action('wp_head', 'start_post_rel_link', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// Remove emoji stuff
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


################################################
#
#  Enable auto-update support
#
################################################
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'automatic_updates_is_vcs_checkout', '__return_false', 1 );

################################################
#
#  Admin bar options
#
################################################
/* Disable WordPress Admin Bar for all users but admins and editors */
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if( current_user_can('editor') || current_user_can('administrator') ) {
        show_admin_bar(true);
    }
    else{
        show_admin_bar(false);
    }
}

################################################
#
#  Admin bar clean
#
################################################
function remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
    $wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
    $wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
    $wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
    $wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
    $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
//    $wp_admin_bar->remove_menu('site-name');        // Remove the site name menu
//    $wp_admin_bar->remove_menu('view-site');        // Remove the view site link
    $wp_admin_bar->remove_menu('updates');          // Remove the updates link
    $wp_admin_bar->remove_menu('comments');         // Remove the comments link
//    $wp_admin_bar->remove_menu('new-post');         // Remove the content link
    $wp_admin_bar->remove_menu('customize');        // If you use w3 total cache remove the performance link
//    $wp_admin_bar->remove_menu('my-account');       // Remove the user details tab
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );


################################################
#
#  Hide upgrade message from client
#
################################################
add_action('admin_menu','wphidenag');
function wphidenag() {
    remove_action( 'admin_notices', 'update_nag', 3 );
}

function admin_css(){

    global $current_user;

    if ($current_user->ID != 1){

        echo '<style>.update-nag, #wp-version-message a.button { display:none; } #dashboard_recent_comments, #dashboard_quick_press, #dashboard_incoming_links, #dashboard_recent_drafts, #dashboard_primary, #dashboard_secondary, #dashboard_plugins { display:none!important; }</style>';

    }

}


################################################
#
#  SVG uploads
#
################################################
function cc_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter( 'upload_mimes', 'cc_mime_types' );


################################################
#
#  Custom post types
#
################################################
//function activities() {
//    register_post_type( 'activities', array(
//        'labels' => array(
//            'name' => 'Activities',
//            'singular_name' => 'Activity',
//        ),
//
//        'public' => true,
//        'menu_icon' => 'dashicons-art',
//        'exclude_from_search' => false,
//        'show_ui' => true,
//        'rewrite' => array('slug' => 'activities','with_front' => false),
//        'supports' => array( 'title' ,'thumbnail', 'editor', 'revisions' ),
//    ) );
//}
//add_action( 'init', 'activities' );

// OR LONG VERSION

// Register Custom Post Type
//function custom_post_type() {
//
//    $labels = array(
//        'name'                  => _x( 'Post Types', 'Post Type General Name', 'text_domain' ),
//        'singular_name'         => _x( 'Post Type', 'Post Type Singular Name', 'text_domain' ),
//        'menu_name'             => __( 'Post Types', 'text_domain' ),
//        'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
//        'archives'              => __( 'Item Archives', 'text_domain' ),
//        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
//        'all_items'             => __( 'All Items', 'text_domain' ),
//        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
//        'add_new'               => __( 'Add New', 'text_domain' ),
//        'new_item'              => __( 'New Item', 'text_domain' ),
//        'edit_item'             => __( 'Edit Item', 'text_domain' ),
//        'update_item'           => __( 'Update Item', 'text_domain' ),
//        'view_item'             => __( 'View Item', 'text_domain' ),
//        'search_items'          => __( 'Search Item', 'text_domain' ),
//        'not_found'             => __( 'Not found', 'text_domain' ),
//        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
//        'featured_image'        => __( 'Featured Image', 'text_domain' ),
//        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
//        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
//        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
//        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
//        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
//        'items_list'            => __( 'Items list', 'text_domain' ),
//        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
//        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
//    );
//    $args = array(
//        'label'                 => __( 'Post Type', 'text_domain' ),
//        'description'           => __( 'Post Type Description', 'text_domain' ),
//        'labels'                => $labels,
//        'supports'              => array( ),
//        'taxonomies'            => array( 'category', 'post_tag' ),
//        'hierarchical'          => false,
//        'public'                => true,
//        'show_ui'               => true,
//        'show_in_menu'          => true,
//        'menu_position'         => 5,
//        'show_in_admin_bar'     => true,
//        'show_in_nav_menus'     => true,
//        'can_export'            => true,
//        'has_archive'           => true,
//        'exclude_from_search'   => false,
//        'publicly_queryable'    => true,
//        'capability_type'       => 'page',
//    );
//    register_post_type( 'post_type', $args );
//
//}
//add_action( 'init', 'custom_post_type', 0 );

################################################
#
#  Hide menus from client
#
################################################
add_action( 'admin_menu', 'remove_menu_pages' );

function remove_menu_pages(){

    global $current_user;

    if ($current_user->ID != 1){

        remove_menu_page('edit-comments.php');

        remove_submenu_page('themes.php','themes.php');

        remove_submenu_page('themes.php','customize.php');

        remove_submenu_page('themes.php','widgets.php');

        remove_submenu_page('themes.php','theme-editor.php');

        remove_menu_page('tools.php');

        remove_menu_page('plugins.php');

        remove_submenu_page('index.php','update-core.php');

        remove_menu_page('options-general.php');

        // Hide ACF from menu for client
        remove_menu_page( 'edit.php?post_type=acf-field-group' );

    }

//    remove_menu_page('edit.php');

}

add_action( 'admin_head', 'admin_css' );


################################################
#
#  Change login screen
#
################################################
add_action("login_head", "client_logo");

function client_logo() {
    echo "
    <style>
    body.login { background:#eee; }
    body.login #login h1 a {
        background: url('".get_bloginfo('template_url')."/assets/images/logo.png') no-repeat scroll center top transparent;
            background-size: contain;
            height: 120px;
        width: 320px;margin:0;
    }
    .login #nav a, .login #backtoblog a { color:#444!important; text-shadow:none!important; }
    .wp-core-ui .button-primary { background:#dd5136!important; border:none; text-shadow:none; }
    </style>
    ";
}


################################################
#
#  Custom excerpt
#
################################################
//function custom_wp_trim_excerpt($text) {
//    $raw_excerpt = $text;
//    if ( '' == $text ) {
//        $text = get_the_content('');
//        $text = strip_shortcodes( $text );
//
//        $text = preg_replace("/<img[^>]+\>/i", " ", $text);
//        $text = apply_filters('the_content', $text);
//        $text = str_replace(']]>', ']]>', $text);
//
//        $text = apply_filters('the_content', $text);
//        $text = str_replace(']]>', ']]>', $text);
//        $excerpt_length = apply_filters('excerpt_length', 20);
//        $excerpt_more = apply_filters( 'excerpt_more', ' ' . '...' );
//        $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
//        if ( count($words) > $excerpt_length ) {
//            array_pop($words);
//            $text = implode(' ', $words);
//            $text = $text . $excerpt_more;
//            $text = force_balance_tags( $text );
//        } else {
//            $text = implode(' ', $words);
//        }
//    }
//    return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
//}
//remove_filter('get_the_excerpt', 'wp_trim_excerpt');

################################################
#
#  Remove comments from CMS
#
################################################
// Removes from admin menu

add_action( 'admin_menu', 'my_remove_admin_menus' );

function my_remove_admin_menus() {

    remove_menu_page( 'edit-comments.php' );

}

// Removes from post and pages

add_action('init', 'remove_comment_support', 100);


function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}

// Removes from admin bar

function mytheme_admin_bar_render() {

    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('comments');

}

add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );


/**
 * Changing failed logins to 401s allows nginx to log auth failures
 */
function fail2ban_login_failed_401() {
    status_header( 401 );
}
add_action( 'wp_login_failed', 'fail2ban_login_failed_401' );



/**
 * Move Yoast to the Bottom
 */
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');



/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );
/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );
/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );
/** Register all navigation menus */
require_once( 'library/navigation.php' );
/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );
/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );
/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );
/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );
/** Add theme support */
require_once( 'library/theme-support.php' );
/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );
/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );
/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );
/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );