<?php
/*
Template Name: Contact Template
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <section class="section hero">
        <div class="row">

            <div class="small-12 columns">
                <h1><?php the_title(); ?></h1>
            </div>

        </div>
    </section>

    <section class="section">
        <div class="row align-center">

            <div class="small-10 small-centered large-8 columns">

                <div class="row">
                    <div class="small-12 medium-6 columns">
                        <?php the_content(); ?>
                    </div>

                    <div class="small-12 medium-6 columns form">
                        <?php // echo do_shortcode('[contact-form-7 id="#" title="Contact Us"]');?>
                    </div>

                </div>

            </div>

        </div>
    </section>
<?php endwhile;?>


<?php get_footer();
