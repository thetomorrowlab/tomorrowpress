<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

    <section id="single-post" class="row" role="main">

        <?php while ( have_posts() ) : the_post(); ?>
            <article class="columns small-12 medium-8" id="post-<?php the_ID(); ?>">
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>

                <div class="entry-content">

                    <?php
                    if ( has_post_thumbnail() ) :
                        the_post_thumbnail();
                    endif;
                    ?>

                    <?php the_content(); ?>

                </div>
                <footer>
                    <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
                </footer>
                <?php the_post_navigation(); ?>

            </article>
        <?php endwhile;?>

        <div class="columns small-12 medium-4">
            <?php get_sidebar(); ?>
        </div>

    </section>

    <script type="application/ld+json">
    <?php
        $jsonData = [
            '@content' => 'http://schema.org',
            '@type' => 'Article',
            'headline' => get_the_title(),
            'description' =>  get_the_excerpt(),

            'url' => get_permalink(),
            'datePublished' => get_the_date('c')
        ];
        if(has_post_thumbnail()) {
            $jsonData['image'] = get_the_post_thumbnail_url();
        }
        echo json_encode($jsonData);
        ?>
  </script>


<?php get_footer();
