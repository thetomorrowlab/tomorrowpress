<?php
/*
Template Name: Homepage Template
*/
get_header();
while ( have_posts() ) : the_post();
    ?>

    <section class="section hero-home" style="background-image:url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url('featured-xlarge');} ?>);">

        <div class="row">
            <div class="columns small-12">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </div>
        </div>

    </section>


    <section class="section content">

        <div class="row">
            <div class="columns small-12">
                <?php the_content(); ?>
            </div>
        </div>

    </section>

    <section class="section content">

        <div class="row">
            <?php get_template_part('parts/tpl-part-posts'); ?>
        </div>

    </section>

    <?php

endwhile;

get_footer();
