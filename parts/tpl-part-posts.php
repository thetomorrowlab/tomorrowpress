<?php
// SHOW POSTS
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = array(
    'posts_per_page' => 8,
    'paged' => $paged
);

$noticeboard = new WP_Query( $args );
while ( $noticeboard->have_posts() ) : $noticeboard->the_post(); ?>
    <div class="columns small-12 medium-6 large-3">
        <a class="post-item" href="<?php the_permalink();?>">
            <?php
            if ( has_post_thumbnail() ) {
                the_post_thumbnail('featured-small');
            }
            ?>
            <h3><?php the_title();?></h3>
            <p><?php the_excerpt();?></p>
            <p class="post-author">Added by: <?php the_author();?></p>
            <p class="post-date"><?php the_time( get_option( 'date_format' ) ); ?></p>
            <?php foreach(get_the_category() as $category) {
            ?><p class='category-tag <?php echo $category->slug;?>'><?php echo $category->slug . ' </p>';} ?>
        </a>
    </div>
    <?php
endwhile;
wp_reset_query();
?>