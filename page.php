<?php
/**
 * The template for displaying pages
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <section class="section hero" style="background-image:url(<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url('featured-xlarge');} ?>);">

        <div class="row">

            <div class="small-12 columns">
                <h1><?php the_title(); ?></h1>
            </div>

        </div>
    </section>

    <section class="section">

        <div class="row">
            <div class="small-12 medium-6 columns">
                <?php the_content(); ?>
            </div>

            <div class="small-12 medium-6 columns">

            </div>

        </div>

    </section>
    <?php
endwhile;
get_footer();
