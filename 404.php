<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header(); ?>

<section>
    <div class="row">
        <div class="small-12 large-8 columns" role="main">

            <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                <header>
                    <h1 class="entry-title">Page Not Found</h1>
                </header>
                <div class="entry-content">
                    <div class="error">
                        <p class="bottom">The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
                    </div>

                </div>
            </article>

        </div>
        <?php get_sidebar(); ?>
    </div>
</section>
<?php get_footer();
