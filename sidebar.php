<?php
/**
 * The sidebar containing the main widget area
 */

?>
<aside class="sidebar">
    <?php dynamic_sidebar( 'sidebar-widgets' ); ?>
</aside>
