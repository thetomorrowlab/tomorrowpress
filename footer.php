<?php
/**
 * The template for displaying the footer
 */
?>

	</section>
	<div id="footer-container">
		<footer id="footer">

			<div class="row">
				<div class="columns small-12">
					<p>© <?php get_bloginfo( 'name' ); ?> <?php echo date("Y"); ?></p>
					<p>Website design by <a href="https://www.thetomorrowlab.com">The Tomorrow Lab</a></p>
				</div>
			</div>

		</footer>
	</div>

<?php wp_footer(); ?>

</body>
</html>
