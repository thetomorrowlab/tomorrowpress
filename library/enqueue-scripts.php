<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_scripts' ) ) :
    function foundationpress_scripts() {

        // Enqueue the main Stylesheet.
        $foundation_mod_time = filemtime(get_template_directory().'/assets/stylesheets/foundation.css');
        wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/stylesheets/foundation.css', array(), $foundation_mod_time, 'all' );

        // Deregister the jquery version bundled with WordPress.
        wp_deregister_script( 'jquery' );

        // CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
        wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );

        $modernizer_mod_time = filemtime(get_template_directory().'/assets/javascript/modernizr.js');
        wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/javascript/modernizr.js#asyncload', array(), $modernizer_mod_time, true );

        $mod_time = filemtime(get_template_directory().'/assets/javascript/foundation.js');
        wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), $mod_time, true );

    }


    add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;