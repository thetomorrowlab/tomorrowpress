<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header id="masthead" class="site-header" role="banner">
    <div class="title-bar" data-responsive-toggle="site-navigation">
        <div class="title-bar-title">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <img src="<?php echo THEME_URL;?>/assets/images/logo.png" alt="<?php bloginfo( 'name' ); ?>"/>
            </a>
        </div>
        <button class="menu-icon" type="button" data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button>
    </div>

    <nav id="site-navigation" class="main-navigation top-bar" role="navigation">
        <div class="top-bar-left">
            <ul class="menu">
                <li class="home">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        <img src="<?php echo THEME_URL;?>/assets/images/logo.png" alt="<?php bloginfo( 'name' ); ?>"/>
                    </a>
                </li>
            </ul>
        </div>
        <div class="top-bar-right">
            <?php foundationpress_top_bar_r(); ?>
            <?php get_template_part( 'parts/mobile-top-bar' ); ?>
        </div>
    </nav>
</header>
